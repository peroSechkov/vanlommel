//
//  VehiclesCollectionViewCell.h
//  Vanlommel
//
//  Created by AspidaMacBook on 17/04/2018.
//  Copyright © 2018 AspidaMacBook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VehiclesCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *vehicleImageView;
@property (nonatomic,strong) UILabel *vehicleTitle;
@property (nonatomic,strong) UIView *grayLine;

@end
