//
//  VehiclesCollectionViewCell.m
//  Vanlommel
//
//  Created by AspidaMacBook on 17/04/2018.
//  Copyright © 2018 AspidaMacBook. All rights reserved.
//

#import "VehiclesCollectionViewCell.h"
#import <Masonry.h>

@implementation VehiclesCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
        [self setupConstraints];
    }
    return self;
}

-(void)setupViews {
    _vehicleImageView = [[UIImageView alloc] init];
    _vehicleImageView.contentMode = UIViewContentModeScaleToFill;
    
    _vehicleTitle = [[UILabel alloc] init];
    _vehicleTitle.textColor = [UIColor blackColor];
    _vehicleTitle.textAlignment = NSTextAlignmentCenter;
    
    _grayLine = [[UIView alloc] init];
    _grayLine.backgroundColor = [UIColor lightGrayColor];
    
    [self.contentView addSubview:_grayLine];
    [self.contentView addSubview:_vehicleImageView];
    [self.contentView addSubview:_vehicleTitle];
}

-(void)setupConstraints {
    
    [_grayLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.height.mas_equalTo(0.3);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    
    [_vehicleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.top.equalTo(_grayLine.mas_bottom).offset(15);
        make.bottom.equalTo(_vehicleTitle.mas_top);
    }];
    
    [_vehicleTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(self.contentView).dividedBy(6);
    }];
}

@end
