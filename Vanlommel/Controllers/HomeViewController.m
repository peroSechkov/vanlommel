//
//  HomeViewController.m
//  Vanlommel
//
//  Created by AspidaMacBook on 17/04/2018.
//  Copyright © 2018 AspidaMacBook. All rights reserved.
//

#import "HomeViewController.h"
#import <Masonry.h>
#import "VehiclesCollectionViewCell.h"

@interface HomeViewController () <UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor = [UIColor whiteColor];
    [self setupViews];
    [self setupConstraints];
    // Do any additional setup after loading the view.
    
}

-(void)setupViews {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(self.view.frame.size.width/2 - 15,self.view.frame.size.width/2 - 15);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumInteritemSpacing = 10;
    _collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView registerClass:[VehiclesCollectionViewCell self] forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:_collectionView];
}

-(void)setupConstraints {
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.view);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionView delagate and datasource functions
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    VehiclesCollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.vehicleImageView.image = [UIImage imageNamed:@"test"];
    cell.vehicleTitle.text = @"Test";
    return cell;
}
@end
