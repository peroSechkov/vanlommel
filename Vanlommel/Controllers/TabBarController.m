//
//  TabBarController.m
//  Vanlommel
//
//  Created by AspidaMacBook on 17/04/2018.
//  Copyright © 2018 AspidaMacBook. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTabs];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupTabs {
   
    _homeVC = [[HomeViewController alloc] init];
    _homeVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Home" image:[UIImage imageNamed:@"home"] tag:0];
     UINavigationController *homeNavVC = [[UINavigationController alloc] init];
    [homeNavVC pushViewController:_homeVC animated:false];
    
    _servicesVC = [[ServicesViewController alloc] init];
    _servicesVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Services" image:[UIImage imageNamed:@"home"] tag:1];
    UINavigationController *servicesNavVC = [[UINavigationController alloc] init];
    [servicesNavVC pushViewController:_servicesVC animated:false];
    
    _contactVC = [[ContactViewController alloc] init];
    _contactVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Contact" image:[UIImage imageNamed:@"home"] tag:2];
    UINavigationController *contactNavVC = [[UINavigationController alloc] init];
    [contactNavVC pushViewController:_contactVC animated:false];
    
    _newsVC = [[NewsViewController alloc] init];
    _newsVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"News" image:[UIImage imageNamed:@"home"] tag:3];
    UINavigationController *newsNavVC = [[UINavigationController alloc] init];
    [newsNavVC pushViewController:_newsVC animated:false];
    
    _aboutVC = [[AboutViewController alloc] init];
    _aboutVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"About" image:[UIImage imageNamed:@"home"] tag:4];
    UINavigationController *aboutNavVC = [[UINavigationController alloc] init];
    [aboutNavVC pushViewController:_aboutVC animated:false];
    
    self.viewControllers =  [[NSArray alloc] initWithObjects:homeNavVC,servicesNavVC,contactNavVC,newsNavVC,aboutNavVC, nil];
}

@end
