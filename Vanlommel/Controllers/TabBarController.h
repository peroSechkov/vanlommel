//
//  TabBarController.h
//  Vanlommel
//
//  Created by AspidaMacBook on 17/04/2018.
//  Copyright © 2018 AspidaMacBook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "ServicesViewController.h"
#import "ContactViewController.h"
#import "NewsViewController.h"
#import "AboutViewController.h"

@interface TabBarController : UITabBarController

@property (nonatomic,strong) HomeViewController *homeVC;
@property (nonatomic,strong) ServicesViewController *servicesVC;
@property (nonatomic,strong) ContactViewController *contactVC;
@property (nonatomic,strong) NewsViewController *newsVC;
@property (nonatomic,strong) AboutViewController *aboutVC;

@end
